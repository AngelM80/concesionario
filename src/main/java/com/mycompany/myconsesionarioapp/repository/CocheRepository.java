package com.mycompany.myconsesionarioapp.repository;

import java.util.List;

import com.mycompany.myconsesionarioapp.domain.Coche;
import com.mycompany.myconsesionarioapp.service.dto.CocheDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Coche entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CocheRepository extends JpaRepository<Coche, Long> {
    List<Coche> findAllByColor(String color);

    @Query ("SELECT c FROM Coche c where c.modelo Like :modelo%")  // % indica el Starting With para ir realizando la busqueda por caracteres//
    Page<Coche> cochesPorModelo(@Param("modelo") String modelo, Pageable pageable);

}
