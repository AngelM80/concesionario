package com.mycompany.myconsesionarioapp.repository;

import java.util.List;

import com.mycompany.myconsesionarioapp.domain.Empleado;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;



/**
 * Spring Data SQL repository for the Empleado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {
    Page<Empleado> findByActivo(Boolean activo, Pageable pageable);
}
