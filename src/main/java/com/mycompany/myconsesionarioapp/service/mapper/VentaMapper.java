package com.mycompany.myconsesionarioapp.service.mapper;

import com.mycompany.myconsesionarioapp.domain.Venta;
import com.mycompany.myconsesionarioapp.service.dto.VentaDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Venta} and its DTO {@link VentaDTO}.
 */
@Mapper(componentModel = "spring", uses = { EmpleadoMapper.class, ClienteMapper.class, CocheMapper.class })
public interface VentaMapper extends EntityMapper<VentaDTO, Venta> {
    @Mapping(target = "empleado", source = "empleado", qualifiedByName = "nombre")
    @Mapping(target = "cliente", source = "cliente", qualifiedByName = "nombre")
    @Mapping(target = "coche", source = "coche", qualifiedByName = "marca")
    VentaDTO toDto(Venta s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    VentaDTO toDtoId(Venta venta);
}
