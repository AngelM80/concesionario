package com.mycompany.myconsesionarioapp.service.mapper;

import com.mycompany.myconsesionarioapp.domain.Coche;
import com.mycompany.myconsesionarioapp.service.dto.CocheDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Coche} and its DTO {@link CocheDTO}.
 */
@Mapper(componentModel = "spring", uses = { VentaMapper.class })
public interface CocheMapper extends EntityMapper<CocheDTO, Coche> {
    @Mapping(target = "venta", source = "venta", qualifiedByName = "id")
    CocheDTO toDto(Coche s);

    @Named("marca")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "marca", source = "marca")
    CocheDTO toDtoMarca(Coche coche);
}
