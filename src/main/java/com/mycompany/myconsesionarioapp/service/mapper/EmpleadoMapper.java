package com.mycompany.myconsesionarioapp.service.mapper;

import com.mycompany.myconsesionarioapp.domain.Empleado;
import com.mycompany.myconsesionarioapp.service.dto.EmpleadoDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Empleado} and its DTO {@link EmpleadoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmpleadoMapper extends EntityMapper<EmpleadoDTO, Empleado> {
    @Named("nombre")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "nombre", source = "nombre")
    EmpleadoDTO toDtoNombre(Empleado empleado);
}
