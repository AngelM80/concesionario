/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mycompany.myconsesionarioapp.web.rest.vm;
