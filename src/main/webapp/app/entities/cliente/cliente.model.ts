export interface ICliente {
  id?: number;
  dni?: string | null;
  nombre?: string | null;
  numeroCompra?: number | null;
  tier?: number | null;
}

export class Cliente implements ICliente {
  constructor(
    public id?: number,
    public dni?: string | null,
    public nombre?: string | null,
    public numeroCompra?: number | null,
    public tier?: number | null
  ) {}
}

export function getClienteIdentifier(cliente: ICliente): number | undefined {
  return cliente.id;
}
