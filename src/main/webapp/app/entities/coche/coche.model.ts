import { IVenta } from 'app/entities/venta/venta.model';

export interface ICoche {
  id?: number;
  marca?: string | null;
  modelo?: string | null;
  color?: string | null;
  numeroSerie?: number | null;
  precio?: number | null;
  transpuesto?: number | null;
  venta?: IVenta | null;
}

export class Coche implements ICoche {
  constructor(
    public id?: number,
    public marca?: string | null,
    public modelo?: string | null,
    public color?: string | null,
    public numeroSerie?: number | null,
    public precio?: number | null,
    public transpuesto?: number | null,
    public venta?: IVenta | null
  ) {}
}

export function getCocheIdentifier(coche: ICoche): number | undefined {
  return coche.id;
}
